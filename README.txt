This project is using Spiking Neuron Network(SNN) to learn and test on MNIST dataset.

We added some preselection on the dataset, so that our network can learn better and faster.

Using the ./run.sh file, you can run the whole network, but you have to change the parameters in ./run.sh to get the result you want.

The result will automatically be created in the ./result/ folder.

Here are the list of the parameters:

1.  Vth : The preset voltage theshold of the whole network. Based on the fact that all the neurons have different threshold voltage to fire, we use an initial normal distribution of those threshold.
         
          Which means for every neuron in the network, their threshold is different but in total they follows a normal distribution. The mean and variance is a "hyperparameter".
         
          You can find them in the ./run.sh, Vth_u is the mean and Vth_s is the variance.
         
2.  Epoch/iteration:  The number of times you want the whole data set to be shown to the network. 

3.  Lr:   Initial learning rate. Since we have three layers, we have two learning rate to preset, which is Lr[0], Lr[1], respectively. Because we use adaptive learning rate to improve the 
  
          proformance of the network, the initial learning rate should set to a larger value. We'll explain it in dacay rate part.
          
4.  Syn_u, Syn_s : The initial weights of the synapses. The synapses in our network also follows the Normal distribution. 1,2 means the first and second connection layer.

5.  maxW/minW: The maximum weights of the synapses during the learning process. Since we are aiming to establish a more biological plausible model, we have to follow the truth that:
    
               (i)There's no nagetive weights in reality (The inibitary neurons should not be concerned as negative weights synapse).
               
               (ii)All the synapses can change their weights in a range if we want to learn, but the range varies from synapse to synapse depends on its orginal "weights".
               
               We set two weights constrains, the first one is the global maximum weight constrain, synapse cannot have a weight larger than this value. The second
               
               one is the unique constrain for every neuron. Which we preset the neuron can have a maximum weight twice than its initial weight. And also, all the weights should
               
               above zero. Unfortunately, we do not set the unique weight constrain range as a parameter, if you want to change that, you can go to network.py to find it. It's noted.
               
6.  D ：Decay rate of the learning rate. The ddecrease strategy is like this: we compare our accuracy in this epoch and the one in the last epoch, if we have a higher accuracy nnow, then 
  
        will not change the learning rate. But if it's dropping, then we set new learning rate as: previous learning rate * exp (-D). Although SGD has its stochastic factor, but the trend
         
        of accuracy should still follows the rule that if the learning rate is too large near the answer point, the accuracy will fluctuate a lot, or even go down.
         
        We set two differnt decay rate for the two layers, we have to preset them according to the result of the experiment.
         
7.  Number : The size of the hidden layer. Of course that having a large number of neurons in the single hidden layer will have a better accuracy, we set the number as 100 to get our result.

8.  Density : More precisely, the connectvity percentage in the hidden layer. Which means how many post-synapses does a neuron have comparing to the number of neurons in the next layer. 
      
              In real neural network this value is really low, much lower than 40% ---which we set only for experiment.
            
About the preselection of the MNIST dataset. We use some method to let the original figure to have more similar number of "ones" --- the black pixels in the figure, in order to let the 
    
network learn better. You can see what we done in the paper.

