import random
import math
import numpy as np


def softmax(x):
    e_x = np.exp(x - np.max(x))
    return e_x / e_x.sum()


class Network(object):

    def __init__(self, sizes, initalW, maxW, Vth, percent, minW):
        self.num_layers = len(sizes)
        self.sizes = sizes
        self.maxW = maxW
        self.correct_test = 0
        self.correct_train = 0
        self.totalNum = 0
        self.minW = minW
        self.meanWeight=0
        self.last_accuracy=0

        np.random.seed(2)
        self.Vth = Vth[0] + np.random.randn(sizes[1], 1) * Vth[1]

        # weight initialization
        self.weights = []
        for s in range(self.num_layers - 1):
            np.random.seed(2)
            self.weights.append(initalW[s] + np.random.randn(sizes[s + 1], sizes[s]) * initalW[s + 2])
            if s != self.num_layers - 2:
                self.weights[s] = self.weights[s].clip(min=minW)
                self.weights[s] = self.weights[s].clip(max=maxW)

                # random connections
                mask = np.random.randint(0, 100, size=self.weights[s].shape).astype(np.int)
                mask[mask < percent] = 1
                mask[mask >= percent] = 0
                print "connection percentage:", float(np.sum(mask)) / (self.sizes[0] * sizes[s + 1])
                self.weights[s] = self.weights[s] * mask

        self.mask = mask

    def SGD(self, param, param1, epochs, mini_batch_size, lr, train_data, test_data=None):

        for i in xrange(epochs):
            # training
            mini_batches = [train_data[j:j + mini_batch_size] for j in xrange(0, len(train_data), mini_batch_size)]
            zz = 0
            for mini_batch in mini_batches:
                self.evaluate(mini_batch, 1)
                zz += self.update_mini_batch(mini_batch, lr)
            # print "train Epoch {0}: {1}".format(i, self.evaluate(test_data)/float(n_train))
            # testing
            zz = zz / 1000
            if test_data:
                # print "test Epoch {0}: {1}".format(i, self.evaluate(test_data)/float(len(test_data)))
                accur = self.evaluate(test_data, 0) / float(len(test_data))
                if accur < self.last_accuracy:
                    lr[0] = lr[0] * np.exp(-param[11][0])
                    lr[1] = lr[1] * np.exp(-param[11][1])
                self.last_accuracy = accur
                with open("./result/" + str(param1), "a") as f:
                    data = f.write("epochs: %d" % i)
                    data = f.write(" accuracy: %f" % accur)
                    data = f.write(" SNnumber: %f" % zz)
                    data = f.write(" meanweight: %f\n" %self.meanWeight)
                print accur
                print lr[0]

    def feedforward(self, a):
        count = 0
        for w in self.weights:
            count += 1
            if count == self.num_layers - 1:
                a = softmax(np.dot(w, a))
            else:
                a = np.dot(w, a)
                for n in range(len(a)):
                    if a[n] < self.Vth[n]:
                        a[n] = 0
                    else:
                        a[n] = 1
        return a

    def update_mini_batch(self, mini_batch, lr):
        nabla_w = [np.zeros(w.shape) for w in self.weights]

        for x, y in mini_batch:
            delta_nabla_w, zz = self.backprop(x, y)
            nabla_w = [nw + dnw for nw, dnw in zip(nabla_w, delta_nabla_w)]

        for s in range(self.num_layers - 1):

            self.weights[s] = [w - (lr[s] / len(mini_batch)) * nw for w, nw in zip(self.weights[s], nabla_w[s])]
            self.weights[s] = np.array(self.weights[s])
            if s != self.num_layers - 2:
                self.weights[s] = self.weights[s].clip(min=self.minW)
                self.weights[s] = self.weights[s].clip(max=self.maxW)

                # random connections
                self.weights[s] = self.mask * self.weights[s]

        #---------calculate mean_weight of layer 1-----------
        sumw=0
        count=0
        for swx in range(len(self.weights[0])):
            for swy in range(len(self.weights[0][swx])):
                if(self.mask[swx][swy]==1):
                   sumw += self.weights[0][swx][swy]
                   count += 1

        meanw=sumw/count
        self.meanWeight=meanw
        #----------------------------------------------------

        return zz

    def backprop(self, x, y):
        nabla_w = [np.zeros(w.shape) for w in self.weights]

        activation = x
        activations = [x]  # list to store all the activations, layer by layer
        zs = []  # list to store all the z vectors, layer by layer
        count = 0
        
        for w in self.weights:
            count += 1
            z = np.dot(w, activation)
            if count == self.num_layers - 1:
                activation = softmax(z)
            else:
                sp = np.ones(z.shape)
                for n in range(len(z)):
                    if (z[n]<0.0084) and (z[n]>0.0044):
                        sp[n]=1
                    else:
                        sp[n]=0
                    if z[n] < self.Vth[n]:
                        z[n] = 0
                    else:
                        z[n] = 1

                activation = z
            activations.append(activation)
            zs.append(z)

        # backward pass
        # last layer
        delta = self.cost_derivative(activations[-1], y)  # loss2
        nabla_w[-1] = np.dot(delta, activations[-2].transpose())

        # other layers
        # l = 1 means the last layer of neurons, l = 2 is the second-last layer, and so on. 
        for l in xrange(2, self.num_layers):
            z = zs[-l]
              # straight through estimator
            delta = np.dot(self.weights[-l + 1].transpose(), delta) * sp  # loss1
            nabla_w[-l] = np.dot(delta, activations[-l - 1].transpose())
        zz = sum(z)
        zz = zz[0]
        return nabla_w, zz

    def evaluate(self, test_data, flag):
        test_results = [(np.argmax(self.feedforward(x)), y)
                        for (x, y) in test_data]

        if flag:  # train
            self.totalNum += 1
            if test_results[0][0] == list(test_results[0][1]).index(1):
                self.correct_train += 1
        # print self.totalNum, test_results[0][0], list(test_results[0][1]).index(1), self.correct_train
        else:  # test
            for i in range(len(test_results)):
                if test_results[i][0] == test_results[i][1]:
                    self.correct_test += 1
            # print test_results[i], self.correct_test

            return sum(int(x == y) for (x, y) in test_results)

    def cost_derivative(self, output_activations, y):
        return (output_activations - y)


