# contribute by YUAN mar16/2019
# density, Vth and weight variation, train/test

# overlap selecting.

import scipy.io as sio
import sys
import numpy as np
import pickle as cPickle
import gzip
from network import Network
import random
import matplotlib.pyplot as plt
#from PIL import Image


#--------Settings----------------------------------------------------------
input_neu = 28*28
#extra_neu_unit=7*7
#num_unit=2
datalength = 1000
#------overlap processing---------
unit=2
stride=2#
pd=3
expect_ones=22

line=125  #threshold_initial randomly set
num_trans=1000
do=1

name = './param/photos_train_28_255.mat'
vals = sio.loadmat(name)
images = vals['images']
answer = './param/photos_train_label.mat'
v = sio.loadmat(answer)
labels = v['labels']

#####--------------------------Functions----------------------------------
#-------count ones-------------------------------------------------------------
def count_ones(nimages, index1):
    # type: (object, object) -> object

    count = []
    for j in range(len(nimages)):
        k = 0
        for m in range(len(nimages[j])):
            k = k + nimages[j][m]
        count.append(k)

    sum = 0
    for j in range(len(count)):
        sum = sum + count[j]
    av = sum / len(count)
    print "average: ", av

    average = []
    for num in range(10):
        c = 0
        sum = 0
        for i in range(len(index1)):
            if num == index1[i]:
                sum = sum + count[i]
                c = c + 1
        average.append(int((sum / c)))  # want to add in
        print"num:", num, " ", sum / c

    return av
#-------load MNist--------------------------------------------------------------
def load_mnist(name, datalength):
    vals = sio.loadmat(name)
    images = vals['images']
    return images[0:datalength]

def pixels_to_binary(images,line):
    for i in range(len(images)):
        for j in range(len(images[i])):
            if images[i][j]>line:
                images[i][j]=1
            else:
                images[i][j]=0
    return images

def trans_data_structure(datalength, nimages,n1,n2):
    # --- nimages is ndarry matrix, n1n2 is the shape you want ,output is mix list and ndarry(to functions)
    new_list=[]
    for i in range(datalength):
        new_list.append(nimages[i].reshape(n1,n2))
    return new_list


########--------Functions over----------------------------------------------------------


# ----process MNIST index and load(output type:ndarry)--------------------------------------------------------------
index = []
index1 = []
for i in range(datalength):
    labellist = []
    for j in range(10):
        if labels[0][i] == j:
            value = []
            value.append(1)
            labellist.append(value)
        else:
            value = []
            value.append(0)
            labellist.append(value)

    index1.append(labels[0][i])
    index.append(labellist)
index = np.array(index)
index1 = np.array(index1)

nimages =load_mnist(name, datalength)
# --------------count"1"---------------------

b_nimages=pixels_to_binary(nimages,128)
av_pre=count_ones(b_nimages, index1)
#print"firing rate1:", av_pre/input_neu

##########-------------Encoding-----------------------------------------------------------------------
'''
#------------method 1 add ones----------------
encode=np.ones((len(nimages),extra_neu_unit))
avnp=encode
'''

#------------method 2 add compressing pictures---------------

'''
name = './param/photos_train_7.mat'
datalength = 1000
encode=load_mnist(name,datalength,extra_neu_unit)

avnp=encode
for i in range(num_unit-1):
    avnp=np.hstack((avnp,encode))


debug=0

#------------------connect avnp-----------------------
slice_1000 = images[0:1000]
new_images = np.hstack((slice_1000, avnp))
new_nimages = []  # list

for i in range(datalength):
    new_nimages.append(new_images[i].reshape(input_neu+extra_neu_unit*num_unit, 1))  # list mix with ndarry
total_inputneuron=input_neu+extra_neu_unit*num_unit


'''

#------------method 3 overlaping filter--------------------------


example=load_mnist(name,num_trans)
example=example.reshape(28*num_trans,28)

#im1=Image.fromarray(example)
#im1.show()

#pd--output:examples

examples=np.zeros((28+pd,28+pd))
for k in range(num_trans):
    ex=example[28*k:28*(k+1),:]
    ex=np.pad(ex,((pd,pd),(pd,pd)),'constant')
    if k==0:
        examples=ex
    else:
        examples=np.vstack((examples,ex))

#----------

out_dimension=(np.sqrt(input_neu)+2*pd-unit)/stride+1
out_dimension=int(out_dimension)
print"Expected output dimensions:",out_dimension

output_1=np.zeros((out_dimension,out_dimension))
output_0=np.zeros((out_dimension,out_dimension))

k=0
for i in range(0,out_dimension*stride,stride):
    for j in range(0,out_dimension*stride,stride):
        unit_matrix=examples[i+(k*(14+pd)*2):unit+i+(k*(14+pd)*2),j:j+unit]
        ii=i/stride
        jj=j/stride
        output_0[ii][jj]=np.mean(unit_matrix)


output_sum=output_0

for k in range(num_trans-1):
    for i in range(0,out_dimension*stride,stride):
        for j in range(0,out_dimension*stride,stride):
            unit_matrix=examples[i+((k+1)*(14+pd)*2):unit+i+((k+1)*(14+pd)*2),j:j+unit]
            ii = i / stride
            jj = j / stride
            output_1[ii][jj]=np.mean(unit_matrix)
            '''
            if(output_1[ii][jj]>=line):
                output_1[ii][jj]=255
            else:
                output_1[ii][jj]=0
            '''
    output_sum=np.vstack((output_sum,output_1))



#im2=Image.fromarray(output_sum[:20*out_dimension,:])
#im2.show()
output=output_sum.reshape(num_trans,out_dimension*out_dimension)

#------------find-------------------
flag=1
A1=255
A0=0
av_final=0
count=0
temp=np.zeros(output.shape)

while (flag):
    count+=1
    for i in range(len(output)):
        for j in range(len(output[i])):
            if (output[i][j] >= line):
                temp[i][j] = 255
            else:
                temp[i][j] = 0

    b_output=pixels_to_binary(temp,128)
    av_new=count_ones(b_output,index1[:num_trans])
    if ((av_new>expect_ones-1)and(av_new<expect_ones+1)):
       flag=0
       av_final=av_new
    elif (av_new<=expect_ones-1): # need to decrease av_new
       flag=1
       A1=0.5*(A0+A1)
       line=0.5*(A0+line)
    elif (av_new>=expect_ones+1):
       flag=1
       A0=0.5*(A0+A1)
       line=0.5*(line+A1)
    if (count>=8):
       if(flag):
           print"cannot find the line!"
	   do=0
       break

print"dimensions:", out_dimension
print"the times", count
print"line", line
print"firing ones", av_new
print"firing rate", av_new/(out_dimension*out_dimension)
#--------------Vth

final_pixels=trans_data_structure(datalength,b_output,out_dimension*out_dimension,1)

#------------------------------
encode_training_data = list(zip(final_pixels, index))
encode_validation_data = list(zip(final_pixels, index1))

''''
#--------------------plot-------------------------------------------------------------

plt.figure(figsize=(15, 10))
#plt.grid(True, linestyle = "-.", color = "k")
plt.title('the averge of every number (datalength=1000)'+str(input_neu), loc='left')

n=10
i = np.arange(n)

plt.bar(range(len(average)), average)
for x, y in zip(range(len(average)), average):
    # ha: horizontal alignment
    # va: vertical alignment
    plt.text(x , y + 0.05, '%.2f' % y, ha='center', va='bottom')

plt.xticks(i)
plt.show()

#-----------------------------------------------------------------
'''

# -----------network---------------------------------------------
# 5000 1 196 100 10 0.0035 5 1 1 0.5

epochs = int(sys.argv[1])
batchsize = int(sys.argv[2])
networksize = [out_dimension*out_dimension, int(sys.argv[4]), int(sys.argv[5])]
V = [float(sys.argv[6]), float(sys.argv[7])]
L = [float(sys.argv[8]), float(sys.argv[9])]  # for both layer
U = [float(sys.argv[10]), float(sys.argv[11])]  # for both layer
S = [float(sys.argv[12]), float(sys.argv[13])]  # for both layer
M = float(sys.argv[14])
P = float(sys.argv[15])
D = [float(sys.argv[16]),float(sys.argv[17])]

initialW = [U[0], U[1], S[0], S[1]]

rd = 515
print "File number: ", rd
param = ["pre", rd, V, L, U, S, M, networksize[1], batchsize, P, D]
param1 = ["pre", rd, V, [L[0], L[1]], U, S, M, networksize[1], batchsize, P, D]

with open("./result/" + str(param), "a") as f:
    data = f.write("epochs:%d" % epochs)
    data = f.write(" batchsize:%d" % batchsize)
    data = f.write(" networksize:%s\n" % networksize)
    data = f.write(" initialW:%s\n" % initialW)
    data = f.write("P:%f" % P)
    data = f.write(" V:%s" % V)
    data = f.write(" L:%s" % L)
    data = f.write(" U:%s" % U)
    data = f.write(" S:%s" % S)
    data = f.write(" M:%f" % M)
    data = f.write(" D:%s\n" % D)

''''
#for debugging-------------------------------------------------------------
epochs=50
batchsize=1
networksize=[49,100,10]
V=[0.0064,0.0017]
L=[0.0001,0.0001] #for both layer
U=[0.0008,0.0008] #for both layer
S=[0.0005,0.006] #for both layer
M=0.0045
P=40
initialW=[U[0],U[1],S[0],S[1]]
param=[V,L,U,S,M,networksize[1],batchsize,P]

'''

#max_idea = (V[0] * 2) / (0.01 * P * av)
#print "ideal_maxW: ", max_idea


if (do):
	net = Network(networksize, initialW, M, V, P, 0)
	net.SGD(param, param1, epochs, batchsize, L, encode_training_data, encode_validation_data)

#---------------------------------------------------------------------------
#test_7.py 100 1 233 100 10 0.0058 0.0017 0.00005 0.0008 0.0007 0.0007 0.0007 0.03 0.0014 40
