#randomly select the 2000 images data set from 60000 MNIST training set

import scipy.io as sio
import numpy 
import random as random
import math
from sys import argv
from collections import Counter

#generate random index list
#used the same seed so index are the same for each run, none repeating index
random.seed(a=20)
indexList=random.sample(range(0,59999),2000);

#seed 10, 20

print "max index:", max(indexList)
print "min index:", min(indexList)
print "count max number an index occurs:",max(Counter(indexList).values())

#MNIST dataset////////////////////////////////////////////////////////
name='./photos_train_14.mat'
vals=sio.loadmat(name)
images=vals['images']

answer='./photos_train_label.mat'
v=sio.loadmat(answer)
labels=v['labels']

nimages=[]
index=[]

trainlength=60000
for i in range(trainlength):
	for j in range(196):
		if images[i][j]>0.01:
			images[i][j]=1
		else:
			images[i][j]=0
	
	if i in indexList:	
		nimages.append(images[i])
		index.append(labels[0][i]) 

#store new data set
dataNew = './random2000_image1.mat'
indexNew = './random2000_index1.mat'
sio.savemat(dataNew, {'images':nimages})
sio.savemat(indexNew, {'labels':index})




