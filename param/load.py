#randomly select the 2000 images data set from 60000 MNIST training set

import scipy.io as sio
import numpy 
import random as random
import math
from sys import argv

#radom 2000 MNIST dataset//////////////////////////////////////////////
name='./random2000_image.mat'
vals=sio.loadmat(name)
images=vals['images']

answer='./random2000_index.mat'
v=sio.loadmat(answer)
labels=v['labels']

nimages=[]
index=[]
testingSet=[]
testingLabel=[]
trainingSet=[]
trainingLabel=[]

datalength=2000
for i in range(datalength):
	nimages.append(images[i])
	index.append(labels[0][i]) 

#split data set to train/test/validation
for i in range (2000):
	testingSet=nimages[1600:2000]
	testingLabel=index[1600:2000]
	trainData=nimages[0:1600]
	trainLabel=index[0:1600]

	trainingSet=[trainData[i:i+320] for i in range (0, len(trainData),320)]
	trainingLabel=[trainLabel[i:i+320] for i in range (0, len(trainLabel),320)]

print "testing set size:", len(testingSet)
print "traingSet number:",len(trainingSet)
print "each training set size:", len(trainingSet[0])

#ensure correctness
print trainingLabel[0][0], trainingLabel[1][0], trainingLabel[2][0]
print trainLabel[0], trainLabel[320], trainLabel[640]



